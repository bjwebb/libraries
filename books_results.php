<?php

$isbn = $_REQUEST["book"];
$postcode = $_REQUEST["title"];

$query = "
prefix lib: <http://schemas.talis.com/2005/library/schema#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix dir: <http://schemas.talis.com/2005/dir/schema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix dc: <http://purl.org/dc/terms/>
prefix ov: <http://open.vocab.org/terms/>
prefix bibo: <http://purl.org/ontology/bibo/>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix j.1: <http://rdvocab.info/Elements/>

select ?title ?isbn ?holdingItem ?library_label ?template ?lib_homepage ?cat_homepage ?publisher ?postcode where
{

 ?item bibo:isbn10 \"$isbn\" .
 ?item bibo:isbn10 ?isbn .
 ?item dc:title ?title .
 ?item owl:sameAs ?holdingitem .
 ?item j.1:publishersName ?publisher .

 ?holdingitem lib:heldBy ?lib .
 ?lib lib:isLibraryServiceOf ?auth .
 ?auth ov:containedPostcode ?postcode .
 FILTER ( regex(?postcode, \"1AA$\") ) .
 
 ?lib rdfs:label ?library_label .
 ?lib lib:catalogue ?cat .
 ?cat dir:isbnDeepLinkTemplate ?template .

 OPTIONAL { ?lib foaf:homepage ?lib_homepage }

 OPTIONAL { ?cat foaf:homepage ?cat_homepage }

} LIMIT 10
";

$url = "http://api.talis.com/stores/bbc-librarylink/services/sparql?query=".urlencode($query);
$xml = simplexml_load_file($url);

$data = array();
foreach ($xml->results->result as $result) {
    $data[] = array();
    foreach ($result->binding as $binding) {
        if (!($value = (string)$binding->literal)) $value = (string)$binding->uri;
        $data[sizeof($data)-1][(string)$binding["name"]] = $value;
    }
}

?>
<?='<?xml version="1.0" encoding="UTF-8"?>'?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
	<title>Results</title>
	<link rel="stylesheet" type="text/css" href="http://media.scraperwiki.com/css/main.css" /> 
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style type="text/css">
	  html { height: 100% }
	  body { height: 100%; margin: 0px; padding: 0px }
	  #map_canvas { height: 100% }
	.wibni { background-color: lemonchiffon; }
	</style>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    	<script type="text/javascript"> 
	    var geocoder;
            var map;
            function initialize() {
              geocoder = new google.maps.Geocoder();
              var latlng = new google.maps.LatLng(-34.397, 150.644);
              var myOptions = {
                zoom: 7,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            
                <?php
                    foreach ($data as $datum) {
                        echo "codeAddress('".$datum["postcode"]."')\n";
                    }
                ?>
            }
           
            function codeAddress(address) {
              //var address = "M34 5TU"
              if (geocoder) {
                geocoder.geocode( { 'address': address}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map, 
                        position: results[0].geometry.location
                    });
                  } else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });
              }
            }
	</script>
</head>

<body onload="initialize()">

<div id="divPage"> 
<div class="page_title"> 
<h1 id="hello_world">Results</h1>
<p><?=sizeof($data)?> results near you.</p>
</div>
 
<div class="content">

<ul class="scraper_list"> 

<?php foreach ($data as $datum) { ?>

<li style="margin-top: 10px; height:160px;"> 
<div id="map_canvas" style="width:500px; height:500px; float: right; margin-right: 20px;"></div>
<h3><a href=""><?=$datum["title"];?></a> 
</h3>
<p> 
<strong><a href="<?php str_replace("{isbn}", $datum["isbn"], $datum["template"]); ?>" class="wibni">available now</a></strong> at <a href="<?=$datum["lib_homepage"];?>"><?=$datum["library_label"];?></a>       
</p> 
<small> 
<strong>
ISBN: <?=$datum["isbn"];?><br/>
Publisher: <?=$datum["publisher"];?><br/>
Postcode: <?=$datum["postcode"];?><br/>
<a href="#" class="wibni">see on OpenLibrary</a></strong>
</small>

</li>

<?php } ?>

</ul>



</div>

</div> 


</body>
</html>
