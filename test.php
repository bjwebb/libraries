<?php

$isbn = $_REQUEST["isbn"];
$postcode = $_REQUEST["postcode"];

$query = "
prefix lib: <http://schemas.talis.com/2005/library/schema#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix dir: <http://schemas.talis.com/2005/dir/schema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix dc: <http://purl.org/dc/terms/>
prefix ov: <http://open.vocab.org/terms/>
prefix bibo: <http://purl.org/ontology/bibo/>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix j.1: <http://rdvocab.info/Elements/>

select ?title ?isbn ?holdingItem ?library_label ?template ?lib_homepage ?cat_homepage ?publisher ?postcode where
{

 ?item bibo:isbn10 \"$isbn\" .
 ?item bibo:isbn10 ?isbn .
 ?item dc:title ?title .
 ?item owl:sameAs ?holdingitem .
 ?item j.1:publishersName ?publisher .

 ?holdingitem lib:heldBy ?lib .

 ?lib lib:isLibraryServiceOf ?auth .
 
 ?auth ov:containedPostcode ?postcode .
 ?lib rdfs:label ?library_label .
 ?lib lib:catalogue ?cat .
 ?cat dir:isbnDeepLinkTemplate ?template .

 OPTIONAL { ?lib foaf:homepage ?lib_homepage }

 OPTIONAL { ?cat foaf:homepage ?cat_homepage }

} LIMIT 10
";

$url = "http://api.talis.com/stores/bbc-librarylink/services/sparql?query=".urlencode($query);
$xml = simplexml_load_file($url);

$data = array();
foreach ($xml->results->result as $result) {
    $data[] = array();
    foreach ($result->binding as $binding) {
        if (!($value = (string)$binding->literal)) $value = (string)$binding->uri;
        $data[sizeof($data)-1][(string)$binding["name"]] = $value;
    }
}

?>

	<style type="text/css">
	  #map_canvas { height: 500px; width: 800px; }
	</style> 
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
    	<script type="text/javascript"> 
	    var geocoder;
            var map;
            function initialize() {
              geocoder = new google.maps.Geocoder();
              var latlng = new google.maps.LatLng(-34.397, 150.644);
              var myOptions = {
                zoom: 10,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            
                <?php
                    foreach ($data as $datum) {
                        echo "codeAddress('".$datum["postcode"]."')\n";
                    }
                ?>
            }
           
            function codeAddress(address) {
              //var address = "M34 5TU"
              if (geocoder) {
                geocoder.geocode( { 'address': address}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map, 
                        position: results[0].geometry.location
                    });
                  } else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });
              }
            }
	</script>
<body onload="initialize()">


        <div id="map_canvas"></div>

<?php
#echo "<pre>".htmlspecialchars($query)."</pre>";
#echo "<a href=\"$url\">Test</a>";


echo "<pre>"; print_r($data); echo "</pre>";

?>